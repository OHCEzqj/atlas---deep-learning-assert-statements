testCountryType,createTypeSystemDescription,createCas,setDocumentText,getTypeSystem,getFeatureByBaseName,createAnnotation,setStringValue,addFsToIndexes,setFeatureValue,ConstraintsGrammar,FileInputStream,Parse,ParserVisitor,ValuesGenerator,generatePossibleValues,LinkedList,PossibleValue,getImports,areThereRules,getShortName,getScopeByName,getRules,ruleTriggers,getRestrictions,isFlagImportant,
METHOD_0,METHOD_1,METHOD_2,METHOD_3,METHOD_4,METHOD_5,METHOD_6,METHOD_7,METHOD_8,METHOD_9,METHOD_10,METHOD_11,METHOD_12,METHOD_13,METHOD_14,METHOD_15,METHOD_16,METHOD_17,METHOD_18,METHOD_19,METHOD_20,METHOD_21,METHOD_22,METHOD_23,METHOD_24,METHOD_25,

uima,TypeSystemDescription,tsd,fit,TypeSystemDescriptionFactory,cas,CAS,CasCreationUtils,TypeSystem,ts,continentType,Feature,continentName,AnnotationFS,asiaContinent,countryType,countryName,russia,continentFeature,tudarmstadt,ukp,clarin,webanno,constraints,grammar,ConstraintsGrammar,syntaxtree,Parse,ParsedConstraints,visitor,evaluator,Evaluator,constraintsEvaluator,PossibleValue,possibleValues,exValues,FeatureStructure,imports,parsedConstraints,aContext,aFeature,shortTypeName,scope,Rule,Restriction,pv,
IDENT_0,IDENT_1,IDENT_2,IDENT_3,IDENT_4,IDENT_5,IDENT_6,IDENT_7,IDENT_8,IDENT_9,IDENT_10,IDENT_11,IDENT_12,IDENT_13,IDENT_14,IDENT_15,IDENT_16,IDENT_17,IDENT_18,IDENT_19,IDENT_20,IDENT_21,IDENT_22,IDENT_23,IDENT_24,IDENT_25,IDENT_26,IDENT_27,IDENT_28,IDENT_29,IDENT_30,IDENT_31,IDENT_32,IDENT_33,IDENT_34,IDENT_35,IDENT_36,IDENT_37,IDENT_38,IDENT_39,IDENT_40,IDENT_41,IDENT_42,IDENT_43,IDENT_44,IDENT_45,

56,62,
INT_0,INT_1,




"src/test/resources/rules/region.rules","Asia is the largest continent on Earth. Asia is subdivided into 48 countries, two of them (Russia and Turkey) having part of their land in Europe. The most active place on Earth for tropical cyclone activity lies northeast of the Philippines and south of Japan. The Gobi Desert is in Mongolia and the Arabian Desert stretches across much of the Middle East. The Yangtze River in China is the longest river in the continent. The Himalayas between Nepal and China is the tallest mountain range in the world. Tropical rainforests stretch across much of southern Asia and coniferous and deciduous forests lie farther north.","de.Continent","Asia","de.Country","Russian Federation","continent","regionType","cold","No import for [","] - Imports are: [",
STRING_0,STRING_1,STRING_2,STRING_3,STRING_4,STRING_5,STRING_6,STRING_7,STRING_8,STRING_9,STRING_10,

























