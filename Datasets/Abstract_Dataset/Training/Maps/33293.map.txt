getTestCas,runPipeline,getAED,
METHOD_0,METHOD_1,METHOD_2,

uima,jcas,JCas,jCas,epfl,bbp,ae,OscarAnnotator,julielab,jules,Chemical,chemicals,OscarAnnotatorTest,typesystem,To,
IDENT_0,IDENT_1,IDENT_2,IDENT_3,IDENT_4,IDENT_5,IDENT_6,IDENT_7,IDENT_8,IDENT_9,IDENT_10,IDENT_11,IDENT_12,IDENT_13,IDENT_14,







"Synthesis of the brown dropwise Hyperbranched Macroligands via Michael Addition of Butyl or Ethyl Acrylate with HPEI. The synthetic procedure for partially EA- or BA-modified HPEI is exemplified for HPEI25K-EA0.79: 1.00 g of HPEI25K (Mn = 2.50 x 104, 23.3 mmol of amine groups) was dissolved in 5.00 mL of THF, and then 2.52 mL (23.3 mmol) of EA was added. The mixture was stirred at room temperature for 24 h and subsequently at 50 C for another 24 h. 6 wt. % at 333 K.","matched chemicals",
STRING_0,STRING_1,

























