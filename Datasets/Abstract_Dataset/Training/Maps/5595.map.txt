testGetNegativeTtl,parseInt,getAddressCacheNegativeTtl,
METHOD_0,METHOD_1,METHOD_2,

AddressUtilTest,expectedTtl,expectException,Security,SecurityException,NumberFormatException,AddressUtil,UnknownHostException,negativeTtl,originalException,
IDENT_0,IDENT_1,IDENT_2,IDENT_3,IDENT_4,IDENT_5,IDENT_6,IDENT_7,IDENT_8,IDENT_9,







"Checking that we can get the ttl on dns failures.","networkaddress.cache.negative.ttl","We can't set the DNS cache period, so we're only testing fetching the system value.","Security manager won't let us fetch the property, testing default path.","property is set to 'forever', testing exception path","AddressUtil is (hopefully) going to spit out an error about DNS lookups. ","The JVM Security settings cache DNS failures forever. ","In this case we expect an exception but didn't get one.","We only expect to throw an IllegalArgumentException when the JVM ","caches DNS failures forever.","Failed to get JVM negative DNS respones cache TTL due to format problem ","(e.g. this JVM might not have the property). ","Falling back to default based on Oracle JVM 1.4+ (10s)","Failed to get JVM negative DNS response cache TTL due to security manager. ","JVM negative DNS repsonse cache TTL is set to 'forever' and host lookup failed. ","TTL can be changed with security property ","'networkaddress.cache.negative.ttl', see java.net.InetAddress.","JVM specified negative DNS response cache TTL was negative (and not 'forever'). ",
STRING_0,STRING_1,STRING_2,STRING_3,STRING_4,STRING_5,STRING_6,STRING_7,STRING_8,STRING_9,STRING_10,STRING_11,STRING_12,STRING_13,STRING_14,STRING_15,STRING_16,STRING_17,

























