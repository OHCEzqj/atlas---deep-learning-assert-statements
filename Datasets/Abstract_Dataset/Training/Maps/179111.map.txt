testWordInMultipleFieldsQueryWithStopWords5,Span,Attribute,IntegerField,TextField,StringField,DoubleField,ListField,Tuple,getQueryResults,removeIfExists,containsAll,
METHOD_0,METHOD_1,METHOD_2,METHOD_3,METHOD_4,METHOD_5,METHOD_6,METHOD_7,METHOD_8,METHOD_9,METHOD_10,METHOD_11,

attributeNames,keywordTestConstants,ABSTRACT,uci,ics,texera,span,Span,span1,Attribute,schemaAttributes,ATTRIBUTES_MEDLINE,dataflow,keywordmatcher,KeywordPhraseTest,RESULTS,AttributeType,LIST,IField,Tuple,tuple1,expectedResultList,KeywordTestHelper,MEDLINE_TABLE,phrase,expectedResults,SchemaConstants,_ID,PAYLOAD,exactResults,
IDENT_0,IDENT_1,IDENT_2,IDENT_3,IDENT_4,IDENT_5,IDENT_6,IDENT_7,IDENT_8,IDENT_9,IDENT_10,IDENT_11,IDENT_12,IDENT_13,IDENT_14,IDENT_15,IDENT_16,IDENT_17,IDENT_18,IDENT_19,IDENT_20,IDENT_21,IDENT_22,IDENT_23,IDENT_24,IDENT_25,IDENT_26,IDENT_27,IDENT_28,IDENT_29,

26,37,4566015,
INT_0,INT_1,INT_2,




"British medical journal","Significance of milk pH in newborn infants.","4-5839 Dec 2, 1972","Infant Nutritional Physiological Phenomena, Infant, Newborn, Milk","Bottle-fed infants do not gain weight as rapidly as breast-fed babies during the first week of life. This ","weight lag can be corrected by the addition of a small amount of alkali (sodium bicarbonate or trometamol) to ","the feeds. The alkali corrects the acidity of cow's milk which now assumes some of the properties of human breast ","milk. It has a bacteriostatic effect on specific Escherichia coli in vitro, and in infants it produces a stool with"," a preponderance of lactobacilli over E. coli organisms. When alkali is removed from the milk there is a decrease in","life. Its bacteriostatic effect on specific E. coli may be of practical significance in feed preparations where ",
STRING_0,STRING_1,STRING_2,STRING_3,STRING_4,STRING_5,STRING_6,STRING_7,STRING_8,STRING_9,

0.667832788,
FLOAT_0,






















